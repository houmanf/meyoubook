<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/db/db.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/config.php';

$err = null;


if (isset($_GET['pin']) && isset($_GET['ubid'])) {
	$pin = $_GET['pin'];
	$ubid = $_GET['ubid'];
	$ub = db::getUBdoubleCheck($ubid,$pin);
} else {
	if (isset($_POST['remove'])) {
		$pin = $_POST['pin'];
		$ubid = $_POST['ubid'];
		db::removeUB($ubid, $pin);
		$ub = db::getUBdoubleCheck($ubid, $pin);
	}
}
if ($ub==null) {
	header('Location: http://meyoubook.com');
	$err['notFound'] = true;
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="css/manage.css">
<link rel="icon" href="icon/favicon.ico" type="image/x-icon"> 
<title>Removing selling book - MeYouBook</title>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-28582022-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body id="main">
<div id="content"><?php echo file_get_contents('header.html'); ?>

<p class="labelBig">Removing selling book:</p>

<?php if ($err==null) { include 'manageContent.php'; } else { echo '<div id="noBookFound">Sorry, no selling book was found!<div/>'; }?>

</div>
<?php echo file_get_contents('footer.html');?>
</body>
</html>
