<?php

if (isset($_GET['q']))
{
	$q = $_GET['q'];
}

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="description" content="Frequently asked questions" /> <meta name="keywords" content="sell books, buy books, trade books, exchange books, meyoubook, faq, frequently asked questions" />
<link rel="stylesheet" type="text/css" href="css/faq.css">
<link rel="icon" href="icon/favicon.ico" type="image/x-icon"> 
<title>FAQ - MeYouBook</title>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-28582022-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body id="main">
<div id="content"><?php echo file_get_contents('header.html'); ?>

<p class="labelBig">Frequently Asked Questions:</p>

<div id="q1" class="qaC<?php echo ($q == '1') ? ' focus' : '' ?>">
<div class="q">Will my email address be published?</div>
<div class="a">No, your email address is stored in our system. Your
messages to book owners or buyers will be forwarded to their emails
from our email address and not yours. We will also not share your email
address with third parties.</div>
</div>

<div id="q2" class="qaC<?php echo ($q == '2') ? ' focus' : '' ?>">
<div class="q">I can't find my book by its ISBN, what should I do
now?</div>
<div class="a">Sorry about the inconvenience. Please send your book's ISBN to this<?php echo ' <a href="http://www.google.com/recaptcha/mailhide/d?k=01ku3-oWJnbmGiYEj0xFsRWw==&amp;c=vfcSXgfmCd7EYxsjZVNj7PYFZrdC75SgrAqiEr_Q2MM=" onclick="window.open(\'http://www.google.com/recaptcha/mailhide/d?k\07501ku3-oWJnbmGiYEj0xFsRWw\75\75\46c\75vfcSXgfmCd7EYxsjZVNj7PYFZrdC75SgrAqiEr_Q2MM\075\', \'\', \'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=500,height=300\'); return false;" title="Reveal this e-mail address">email address</a> ';?>
so we can add it to our system. Thank you.</div>
</div>

<div id="q9" class="qaC<?php echo ($q == '9') ? ' focus' : '' ?>">
<div class="q">My zip code is not accepted, what should I do now?</div>
<div class="a">Please use the zip code of another area near you. Thank you.</div>
</div>

<div id="q3" class="qaC<?php echo ($q == '3') ? ' focus' : '' ?>">
<div class="q">Is there any way to update the price, the zip code or the
condition of a listed book I am selling?</div>
<div class="a">No, but you can remove it and repost it with updated
information.</div>
</div>

<div id="q4" class="qaC<?php echo ($q == '4') ? ' focus' : '' ?>">
<div class="q">How can I remove a book I am selling from the website?</div>
<div class="a">You can follow the related link in the emails you have
received about that book to remove it.</div>
</div>


<div id="q5" class="qaC<?php echo ($q == '5') ? ' focus' : '' ?>">
<div class="q">How long will a book I am selling stay in the website?</div>
<div class="a">Until either the book's owner removes it manually or ten messages to the book owner remain unanswered.</div>
</div>


<div id="q6" class="qaC<?php echo ($q == '6') ? ' focus' : '' ?>">
<div class="q">Who can use this website?</div>
<div class="a">MeYouBook.com is open to the public. Everyone can sell their books or search for books to buy. However, we are not responsible for verifing buyer and seller legitimacy.</div>
</div>

<div id="q7" class="qaC<?php echo ($q == '7') ? ' focus' : '' ?>">
<div class="q">Why are some features of the website not working?</div>
<div class="a"><a
	href="http://support.google.com/bin/answer.py?hl=en&answer=23852"
	title="Enabling JavaScript" alt="Enabling JavaScript">Enabeling
JavaScript</a> is required for many features of the website to work.</div>
</div>

<div id="q8" class="qaC<?php echo ($q == '8') ? ' focus' : '' ?>">
<div class="q">What is your contact information?</div>
<div class="a">You can contact us at our <a
	href="http://www.google.com/recaptcha/mailhide/d?k=01ku3-oWJnbmGiYEj0xFsRWw==&amp;c=vfcSXgfmCd7EYxsjZVNj7PYFZrdC75SgrAqiEr_Q2MM="
	onclick="window.open('http://www.google.com/recaptcha/mailhide/d?k\07501ku3-oWJnbmGiYEj0xFsRWw\75\75\46c\75vfcSXgfmCd7EYxsjZVNj7PYFZrdC75SgrAqiEr_Q2MM\075', '', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=500,height=300'); return false;"
	title="Reveal this e-mail address">email address</a>.</div>
</div>

<p class="footNote">We recommend using Firefox or Chrome.</p>
</div>
<?php echo file_get_contents('footer.html'); ?>
</body>

</html>
