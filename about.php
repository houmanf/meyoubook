<?php include_once 'config.php';?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="description" content="About us" /> <meta name="keywords" content="sell books, buy books, trade books, exchange books, meyoubook, about" />
<link rel="stylesheet" type="text/css" href="css/about.css">
<link rel="icon" href="icon/favicon.ico" type="image/x-icon">
<title>About - MeYouBook</title>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-28582022-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body id="main">
<div id="content"><?php echo file_get_contents('header.html'); ?>
<div id="about">
<div>
<p class="labelBig">About</p>
<p>We are a group of graduate students at the University of Colorado at
Boulder. We know affording university is tough, so we decided
to develop a website to save students a little bit money by trading their books among themselves.</p>
</div>
<div>
<p class="labelBig">How it works?</p>
<p>You <b>add the book</b> you are selling to the website. Others living around you can find it and send you <b>messages</b>. You will arrage to meet and <b>trade the book</b>.</p>
</div>

<div>
<p class="labelBig">Benefits</p>
<ul id="benefits">
<li>Take a look at books before purchase.</li>
<li>No shipping fee.</li>
<li>No commission.</li>
<li>It's easy!</li>
</ul>
</div>

<div>
<p class="labelBig">Contact us</p>
<p>You can contact us at our <a href="http://www.google.com/recaptcha/mailhide/d?k=01ku3-oWJnbmGiYEj0xFsRWw==&amp;c=vfcSXgfmCd7EYxsjZVNj7PYFZrdC75SgrAqiEr_Q2MM=" onclick="window.open('http://www.google.com/recaptcha/mailhide/d?k\07501ku3-oWJnbmGiYEj0xFsRWw\75\75\46c\75vfcSXgfmCd7EYxsjZVNj7PYFZrdC75SgrAqiEr_Q2MM\075', '', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=500,height=300'); return false;" title="Reveal this e-mail address">email address</a>.</p>
</div>
</div>
</div>
<?php echo file_get_contents('footer.html'); ?>
</body>
</html>
