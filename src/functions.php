<?php
function generatePin($length=50) {
	// makes a random alpha numeric string of a given length
	$length = $length - 10;
	$aZ09 = array_merge(range('A', 'Z'), range('a', 'z'), range(0, 9));
	$pin = time();
	for($c=0; $c < $length; $c++) {
		$pin .= $aZ09[mt_rand(0,count($aZ09)-1)];
	}
	return $pin;
}

function composeEmail($subject,$message,$type,$replyLink,$manageLink) {
	switch ($type) {
		case 'first':
			$randomHash = md5(date("r",time()));
			$msgPlain = $message;
			$msgHtml = str_replace("\n", "<br/>", $msgPlain);

			$headers = "MIME-Version: 1.0\r\n";
			$headers .= "Subject: ".$subject."\r\n";
			$headers .= "From: service@meyoubook.com\r\n";
			$headers .= "Content-Type: multipart/alternative;boundary=".$randomHash."\r\n";

			$msgBeginPlain = "\r\n\r\n--".$randomHash."\r\n";
			$msgBeginPlain .= "Content-type: text/plain;charset=utf-8"."\r\n\r\n";
			$msgBeginPlain .= "Hey,"."\r\n\r\n";
			$msgBeginPlain .= "Someone sent you a message about your ".$subject." book."."\r\n";
			$msgBeginPlain .= "Go to ".$replyLink." to reply."."\r\n";
			$msgBeginPlain .= "Here's the message:"."\r\n";
			$msgBeginPlain .= "--------------------"."\r\n";
			$msgEndPlain = "\r\n--------------------"."\r\n";
			$msgEndPlain .= "Go to ".$replyLink." to reply."."\r\n";
		    ($manageLink != '') ? $msgEndPlain .= "\r\nIf you already sold your book or are not interested in selling it anymore, go to ".$manageLink." to remove it from the website."."\r\n" : '';
			$msgEndPlain .= "Please do not reply to this email."."\r\n\r\n";
			$msgEndPlain .= "MeYouBook Team";

			$msgBeginHtml = "\r\n\r\n--".$randomHash."\r\n";
			$msgBeginHtml .= "Content-type: text/html;charset=utf-8"."\r\n\r\n";
			$msgBeginHtml .= "Hey,"."<br/><br/>";
			$msgBeginHtml .= "Someone sent you a message about your <b>".$subject."</b> book."."<br/>";
			$msgBeginHtml .= "Click <b><a href=\"".$replyLink."\">here to reply</a></b>."."<br/>";
			$msgBeginHtml .= "Here's the message:"."<br/>";
			$msgBeginHtml .= "--------------------"."<br/>";
			$msgEndHtml = "<br/>--------------------"."<br/>";
			$msgEndHtml .= "Click <b><a href=\"".$replyLink."\">here to reply</a></b>."."<br/>";
			($manageLink != '') ? $msgEndHtml .= "<br/>If you already sold your book or are not interested in selling it anymore, click <b><a href=\"".$manageLink."\">here to remove your book</a></b> from the website."."<br/>" : '';
			$msgEndHtml .= "Please do not reply to this email."."<br/><br/>";
			$msgEndHtml .= "MeYouBook Team";
			$msgEndHtml .= "\r\n\r\n--".$randomHash."--\r\n";

			$body = $msgBeginPlain.$msgPlain.$msgEndPlain.$msgBeginHtml.$msgHtml.$msgEndHtml;
			break;

		case 'regular':
			$randomHash = md5(date("r",time()));
			$msgPlain = $message;
			$msgHtml = str_replace("\n", "<br/>", $msgPlain);

			$headers = "MIME-Version: 1.0\r\n";
			$headers .= "Subject: ".$subject."\r\n";
			$headers .= "From: service@meyoubook.com\r\n";
			$headers .= "Content-Type: multipart/alternative;boundary=".$randomHash."\r\n";

			$msgBeginPlain = "\r\n\r\n--".$randomHash."\r\n";
			$msgBeginPlain .= "Content-type: text/plain;charset=utf-8"."\r\n\r\n";
			$msgBeginPlain .= "Hey,"."\r\n\r\n";
			$msgBeginPlain .= "You received a new message about the ".$subject." book."."\r\n";
			$msgBeginPlain .= "Go to ".$replyLink." to reply."."\r\n";
			$msgBeginPlain .= "Here's the message and your communication history:"."\r\n";
			$msgBeginPlain .= "--------------------"."\r\n";
			$msgEndPlain = "\r\n--------------------"."\r\n";
			$msgEndPlain .= "Go to ".$replyLink." to reply."."\r\n";
			($manageLink != '') ? $msgEndPlain .= "\r\nIf you already sold your book or are not interested in selling it anymore, go to ".$manageLink." to remove it from the website."."\r\n" : '';
			$msgEndPlain .= "Please do not reply to this email."."\r\n\r\n";
			$msgEndPlain .= "MeYouBook Team";

			$msgBeginHtml = "\r\n\r\n--".$randomHash."\r\n";
			$msgBeginHtml .= "Content-type: text/html;charset=utf-8"."\r\n\r\n";
			$msgBeginHtml .= "Hey,"."<br/><br/>";
			$msgBeginHtml .= "You received a new message about the <b>".$subject."</b> book."."<br/>";
			$msgBeginHtml .= "Click <b><a href=\"".$replyLink."\">here to reply</a></b>."."<br/>";
			$msgBeginHtml .= "Here's the message and your communication history:"."<br/>";
			$msgBeginHtml .= "--------------------"."<br/>";
			$msgEndHtml = "<br/>--------------------"."<br/>";
			$msgEndHtml .= "Click <b><a href=\"".$replyLink."\">here to reply</a></b>."."<br/>";
			($manageLink != '') ? $msgEndHtml .= "<br/>If you already sold your book or are not interested in selling it anymore, click <b><a href=\"".$manageLink."\">here to remove your book</a></b> from the website."."<br/>" : '';
			$msgEndHtml .= "Please do not reply to this email."."<br/><br/>";
			$msgEndHtml .= "MeYouBook Team";
			$msgEndHtml .= "\r\n\r\n--".$randomHash."--\r\n";

			$body = $msgBeginPlain.$msgPlain.$msgEndPlain.$msgBeginHtml.$msgHtml.$msgEndHtml;
			break;
			
		case 'submit':
			$randomHash = md5(date("r",time()));
			$msgPlain = $message;
			$msgHtml = str_replace("\n", "<br/>", $msgPlain);

			$headers = "MIME-Version: 1.0\r\n";
			$headers .= "Subject: ".$subject."\r\n";
			$headers .= "From: service@meyoubook.com\r\n";
			$headers .= "Content-Type: multipart/alternative;boundary=".$randomHash."\r\n";

			$msgBeginPlain = "\r\n\r\n--".$randomHash."\r\n";
			$msgBeginPlain .= "Content-type: text/plain;charset=utf-8"."\r\n\r\n";
			$msgBeginPlain .= "Hey,"."\r\n\r\n";
			$msgEndPlain = "You are selling your ".$subject." book at www.meyoubook.com."."\r\n";
			$msgEndPlain .= "You will receive messages from people interested in buying it at your email."."\r\n\r\n";
			($manageLink != '') ? $msgEndPlain .= "You can always go to ".$manageLink." to remove your book from the website."."\r\n" : '';
			$msgEndPlain .= "Please do not reply to this email."."\r\n\r\n";
			$msgEndPlain .= "MeYouBook Team";

			$msgBeginHtml = "\r\n\r\n--".$randomHash."\r\n";
			$msgBeginHtml .= "Content-type: text/html;charset=utf-8"."\r\n\r\n";
			$msgBeginHtml .= "Hey,"."<br/><br/>";
			$msgEndHtml = "You are selling your <b>".$subject."</b> book at <b><a href=\"http://www.meyoubook.com\">www.meyoubook.com</a></b>."."<br/>";
			$msgEndHtml .= "You will receive messages from people interested in buying it at your email."."<br/><br/>";
			($manageLink != '') ? $msgEndHtml .= "You can always click <b><a href=\"".$manageLink."\">here to remove your book</a></b> from the website."."<br/>" : '';
			$msgEndHtml .= "Please do not reply to this email."."<br/><br/>";
			$msgEndHtml .= "MeYouBook Team";
			$msgEndHtml .= "\r\n\r\n--".$randomHash."--\r\n";

			$body = $msgBeginPlain.$msgPlain.$msgEndPlain.$msgBeginHtml.$msgHtml.$msgEndHtml;
			break;
	}

	$email = array("headers" => $headers, "body" => $body);
	return $email;
}


?>