<!DOCTYPE html>
<html>
<head>
<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/db/db.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/config.php';

//$subject = $_GET['subject'];
$ubuid = $_GET['ubuid'];
$pin = $_GET['pin'];
if (isset($_GET['pin']) && isset($_GET['ubuid'])) {
	$comInfo = db::getCommunicationInfo($ubuid, $pin);
	if ($comInfo['ubid']!=null) {
		session_start();
		$_SESSION['authorized'] = true;
		$_SESSION['ubuid'] = $ubuid;
		$_SESSION['subject'] = $comInfo["title"]." (".$comInfo["condition"]."/$".$comInfo["price"].")";
		$_SESSION['history'] = $comInfo['history'];
		if ($comInfo['sPin'] == $pin) {
			$_SESSION['bPin'] = $comInfo['bPin'];
			$_SESSION['from'] = $comInfo['sEmail'];
			$_SESSION['to'] = $comInfo['bEmail'];
		} else {
			$_SESSION['ubPin'] = $comInfo['ubPin'];
			$_SESSION['ubid'] = $comInfo['ubid'];
			$_SESSION['sPin'] = $comInfo['sPin'];
			$_SESSION['to'] = $comInfo['sEmail'];
			$_SESSION['from'] = $comInfo['bEmail'];
		}
	} else {
		header('Location: http://www.meyoubook.com');
	}
		 
}
?>

<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="css/email.css">
<link rel="icon" href="icon/favicon.ico" type="image/x-icon"> 
<title>Send Message - MeYouBook</title>
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<script type="text/javascript" src="javascripts/email.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-28582022-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script language="javascript" type="text/javascript">
function imposeMaxLength(Object, MaxLen)
{
  return (Object.value.length <= MaxLen);
}
</script> 

</head>
<body id="main">
<div id="content">
<?php echo file_get_contents('header.html'); ?>
<p class="labelBig">Reply form:</p>
<div id="messageForm">
<div id="sendEmailForm">
<div id="subjectContainer" class="formField"><label class="formLabel">Subject</label><span
	id="subject"><?php echo $_SESSION['subject'];?></span></div>
<div id="messageContainer" class="formField">
<div><label id="messageLabel" class="formLabel">Message</label><textarea
	id="message" onkeypress="return imposeMaxLength(this, <?php echo EMAILBODY_MAX_LEN; ?>);"
	class="inputText" name="message"></textarea></div>
<div id="messageNote" class="note">Please type your message. (max length: <?php echo EMAILBODY_MAX_LEN;?> characters)</div>
</div>
<div><input id="send" class="button" name="send" type="button"
	value="Send"></div>
<div id="messageStatus">Sending...</div>
</div>
</div>
</div>
<?php echo file_get_contents('footer.html'); ?>
</body>
</html>
