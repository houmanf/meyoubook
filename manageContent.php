<div id="sBookInfoContainer"><img id="cover" class="cover" alt=""
	src="<?php echo "covers/".$ub["frontCover"]; ?>">
<table class="bookInfo">
	<tr>
		<td class="l1">Title:</td>
		<td id="title" class="bookInfoItem"><?php echo $ub["title"]; ?></td>
	</tr>
	<tr>
		<td class="l1">Author(s):</td>
		<td id="authors" class="bookInfoItem""><?php echo $ub["authors"]; ?></td>
	</tr>
	<tr>
		<td class="l1">Publisher:</td>
		<td id="publisher" class="bookInfoItem""><?php echo $ub["publisher"]; ?></td>
	</tr>
	<tr>
		<td class="l1">Length:</td>
		<td id="length" class="bookInfoItem"><?php echo $ub["length"];?></td>

	</tr>
	<tr>
		<td class="l1">ISBN:</td>
		<td id="isbn" class="bookInfoItem""><?php echo $ub["isbn10"].', '.$ub["isbn13"]; ?></td>
	</tr>
</table>
</div>

<div class="bBookPriceContainer">
<div class="bBookPriceHeader"><span>Price</span><span>Zip Code</span><span>Condition</span></div>
<form action="manage.php" method="post">
<div class="bBookPriceRow"><span>$<?php echo $ub["price"]; ?></span><span><?php echo $ub["zipcode"]; ?></span><span><?php echo $ub["condition"]; ?></span>
<?php echo ($ub["available"]) ? '<input name="ubid" type="hidden" value="'.$ubid.'"><input name="pin" type="hidden" value="'.$pin.'"><input name="remove" type="submit" value="Remove" id="remove" />' : '<span title="Removed by you" class="removed">Removed</span>';?>
</div>
</form>
</div>
