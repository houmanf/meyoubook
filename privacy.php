<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="description" content="Privacy information" /> <meta name="keywords" content="sell books, buy books, trade books, exchange books, meyoubook, privacy" />
<link rel="stylesheet" type="text/css" href="css/about.css">
<link rel="icon" href="icon/favicon.ico" type="image/x-icon"> 
<title>Privacy - MeYouBook</title>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-28582022-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body id="main">
<div id="content"><?php echo file_get_contents('header.html'); ?>
<div id="about">
<p class="labelBig">Privacy:</p>
<p>We have been trying hard to protect your privacy. Your messages
to book owners or buyers will be forwarded to their email address from our email address in order to protect yours. Therefore,
your email address will never be published.</p>
<p>To protect book owners from receiving spam, there is a <a href="http://en.wikipedia.org/wiki/CAPTCHA" title="CAPTCHA in Wikipedia">CAPTCHA</a> which must be submitted before an email is forwarded to them.</p>
<p>We will never share your information with third parties.</p>
</div>
</div>
<?php echo file_get_contents('footer.html'); ?>
</body>
</html>
