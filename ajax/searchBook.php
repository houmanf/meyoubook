<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/Crawler.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/config.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/db/db.php';

$err = null;

$isbn = $_GET['isbn'];

if (!(strlen($isbn)==10 || strlen($isbn)==13)) {
	$err['isbn'] = true;
}

if ($err==null) {
	$book = db::searchByISBN($isbn);
	if (!$book) {
		$crawler = new Crawler();
		$book = $crawler->getByISBN($isbn);
		if ($book['isbn']!=null && $book["title"]!=null) {
			db::insertBook($book);
		} else {
			$err['notFound'] = 1;
		}
	}
}

if ($err==null) {
	$book = array("isbn" => $book["isbn"], "title" => $book["title"], "authors" => $book["authors"], "publisher" => $book["publisher"], "length" => $book["length"], "frontCover" => "covers/".$book["frontCover"]);
	$bookJSON = json_encode($book);
}


if ($err==null) {
	echo $bookJSON;
} else {
	echo 'false';	// no book found
}

?>