<?php

include_once $_SERVER['DOCUMENT_ROOT']."/db/db.php";
include_once $_SERVER['DOCUMENT_ROOT'].'/config.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/src/recaptchalib.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/src/functions.php';

if (isset($_POST["bSubmit"])) {
	if (isset($_POST["ubid"])) {

		$u = "";
		$ubu = "";
		$sec = "";

		$bEmail = $_POST["bEmail"];
		$message = $_POST["message"];
		// captcha stuff
		$privatekey = RECAP_PRI_KEY;
		$resp = recaptcha_check_answer ($privatekey,
		$_SERVER["REMOTE_ADDR"],
		$_POST["recaptcha_challenge_field"],
		$_POST["recaptcha_response_field"]);

		if ($resp->is_valid) {
		} else {
			$err['captcha'] = true;
		}

		if (strlen($message) == 0) {
			$err['message'] = true;
		}
		elseif (strlen($message) > EMAILBODY_MAX_LEN) {
			$err['message'] = true;
		}

		// email validation
		if (strlen($bEmail) == 0) {
			$err['email'] = true;
		}
		elseif (strlen($bEmail) > EMAIL_MAX_LEN) {
			$err['email'] = true;
		}
		elseif (!filter_var($bEmail, FILTER_VALIDATE_EMAIL)) {
			$err['email'] = true;
		}

		if ($err==null) {

			$ubid = $_POST["ubid"];
			$ub = db::getUB($ubid);
			$subject = $ub["title"]." (".$ub["condition"]."/$".$ub["price"].")";
			$spin = generatePin();
			$bpin = generatePin();
				
			if ($err==null) {
				$ubuid = db::insertUBUandUBUE($bEmail, $ubid, $message, $spin, $bpin);
				if (!$ubuid) {
					$err["insert"] = true;
				}
					
				if ($err==null) {
					$ubPin = $ub["ubPin"];
					$replyLink = DOMAIN."email.php?ubuid=".$ubuid."&pin=".$spin;
					$manageLink = DOMAIN."manage.php?ubid=".$ubid."&pin=".$ubPin;
					$email = composeEmail($subject,$message,'first',$replyLink, $manageLink);
					$success = mail($ub["email"], $subject, $email["body"], $email["headers"]);
					if (!$success) {
						$err["mail"] = true;
					}
				}
			}
		}
	} else {
		$err["server"] = true;
	}
}

if (isset($_POST['send'])) {
	$message = $_POST['message'];

	if (strlen($message) == 0) {
		$err['message'] = true;
	}
	elseif (strlen($message) > EMAILBODY_MAX_LEN) {
		$err['message'] = true;
	}

	if ($err==null) {
		session_start();
		if (isset($_SESSION['sPin'])) {
			$pin = $_SESSION['sPin'];
			$ubPin = $_SESSION['ubPin'];
			$ubid = $_SESSION['ubid'];
			$manageLink = DOMAIN."manage.php?ubid=".$ubid."&pin=".$ubPin;
		} elseif (isset($_SESSION['bPin'])) {
			$pin = $_SESSION['bPin'];
		}
		$replyLink = DOMAIN."email.php?ubuid=".$_SESSION['ubuid']."&pin=".$pin;
		$messageHistory = $message."\r\n--------------------\r\n".$_SESSION['history'];
		$email = composeEmail($_SESSION['subject'],$messageHistory,'regular',$replyLink, $manageLink);
		mail($_SESSION['to'], $_SESSION['subject'], $email["body"], $email["headers"]);
	}
	if ($err==null) {
		db::insertUBUE($_SESSION['ubuid'], $message);
		$_SESSION = array();
		session_destroy();
	}
}

if ($err==null) {
	echo true;
} else {
	echo json_encode($err);
}
?>

