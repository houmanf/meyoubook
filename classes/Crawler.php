<?php
// crawls the web to get information about a particular book
class Crawler {

	private $googleISBNsearchURL = 'https://www.google.com/search?tbo=p&tbm=bks&q=isbn:';

	function __construct() {
		//		self::getByISBN("9780977320615");
	}

	public function getByISBN($isbn) {
		//$homepage = file_get_contents($this->googleISBNsearchURL . $isbn);
		$DOM = new DOMDocument();
		$DOM->loadHTMLFile($this->googleISBNsearchURL . $isbn); // search the book by isbn
		$DOM->validateOnParse = true;
		// get the book's page url
		$tag = $DOM->getElementById("ires");
		if ($tag->childNodes->length > 0) {	// book assiciated with the isbn is found
			$bookURLraw = $tag->getElementsByTagName("a")->item(0)->attributes->item(0)->textContent;
			$bookGoogleID = $this->getbookGoogleID($bookURLraw);
			$bookURL = "http://books.google.com/books?id=" . $bookGoogleID;
			$DOM->loadHTMLFile($bookURL);

			//		echo $bookURLraw . '</br>';
			//		echo $bookURL . '</br>';



			$bookCoverURL = "http://books.google.com/books?id=" . $bookGoogleID . "&printsec=frontcover&img=1&zoom=1&edge=curl";
			$frontCover = time().".jpg";	// front cover file name
			file_put_contents($_SERVER['DOCUMENT_ROOT']."/covers/".$frontCover, file_get_contents($bookCoverURL));

			$bookInfo = $DOM->getElementById("metadata_content_table")->childNodes;

			// reading and storing the book information
			$book = array("isbn" => "", "title" => "", "authors" => "", "publisher" => "", "subjects" => "", "editor" => "", "year" => "", "authors" => "", "cloud" => "", "length" => "", "frontCover" => "", "bookGoogleID" => "");
			$book['title'] = $DOM->getElementById("metadata_content_table")->firstChild->firstChild->nextSibling->firstChild->textContent;
			for ($i = 0; $i < $bookInfo->length - 1 ; $i++) {
				$info = $bookInfo->item($i)->textContent;
				
				if (substr($info, 0, 7)=="Edition") {
					$book['edition']= substr($info, 7, strlen($info));
				}
				if (substr($info, 0, 6)=="Author") {
					$book['authors']= substr($info, 6, strlen($info));
				}
				if (substr($info, 0, 7)=="Authors") {
					$book['authors']= substr($info, 7, strlen($info));
				}
				if (substr($info, 0, 6)=="Editor") {
					$book['editors']= substr($info, 6, strlen($info));
				}
				if (substr($info, 0, 7)=="Editors") {
					$book['editors']= substr($info, 7, strlen($info));
				}
				if (substr($info, 0, 9)=="Publisher") {
					$book['publisher']= substr($info, 9, strlen($info));
				}
				if (substr($info, 0, 4)=="ISBN") {
					$book['isbn']= substr($info, 4, strlen($info));
				}
				if (substr($info, 0, 6)=="Length") {
					$book['length']= substr($info, 6, strlen($info));
				}
				if (substr($info, 0, 7)=="Subject") {
					$book['subjects']= substr($info, 7, strlen($info));
				}
				if (substr($info, 0, 8)=="Subjects") {
					$book['subjects']= substr($info, 8, strlen($info));
				}
			}

			$book['bookGoogleID'] = $bookGoogleID;
			$book['frontCover'] = $frontCover;
			
			return $book;
		} else {
			return false;
		}
	}

	// parse url to exploit value if the book id from it
	private function getbookGoogleID($url) {
		$start = stripos($url, "?id=")+4;
		$end = stripos($url, '&');
		$bookGoogleID = substr($url, $start, $end - $start);
		return $bookGoogleID;
	}

	private function clearBookURL($url) {
		return $clearedBookURL;
	}

	private function clearURL($url, $q) {
		$q  = '&'.$q.'=';
		$chunkedURL = explode($q, $url);
		$clearedURL = $chunkedURL[0] . substr($chunkedURL[1], strpos($chunkedURL[1], "&"));
		return $clearedURL;
	}
}
