<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/config.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/db/db.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/src/recaptchalib.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/src/functions.php';

$err = null;

// captcha stuff
$privatekey = RECAP_PRI_KEY;
if (isset($_POST["recaptcha_challenge_field"])) {
	$resp = recaptcha_check_answer ($privatekey,
	$_SERVER["REMOTE_ADDR"],
	$_POST["recaptcha_challenge_field"],
	$_POST["recaptcha_response_field"]);
	if ($resp->is_valid) {
	} else {
		$err['captcha'] = true;
	}
}

// seller form submitted
// vaptcha validation

if (isset($_POST['sSubmit'])) {

	$isbns = $_POST['sISBN'];
	$email = $_POST['sEmail'];
	$zipcode = $_POST['sZipcode'];
	$price = $_POST['sPrice'];
	$condition = $_POST['sCondition'];
	//	$sSearch = $_POST['sSearch'];

	$temp = explode(', ', $isbns);
	$isbn10 = $temp[0];
	$isbn13 = $temp[1];

	//	if (!is_numeric($isbn10)) {
	//		$err['isbn'] = true;
	//	}
	//	if (!is_numeric($isbn13)) {
	//		$err['isbn'] = true;
	//	}
	// isbn validation
	if (!(strlen($isbn10)==10 || strlen($isbn13)==13)) {
		$err['isbn'] = true;
	}

	// email validation
	if (strlen($email) == 0) {
		$err['email'] = true;
	}
	elseif (strlen($email) > EMAIL_MAX_LEN) {
		$err['emailLong'] = true;
	}
	elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		$err['email'] = true;
	}

	// zipcode validation
	if (strlen($zipcode) <= 0 ||  strlen($zipcode) > 6) {
		$err['sZipcode'] = true;
	} else if (!db::validZipcode($zipcode)) {
		$err['sZipcode'] = true;
	}

	// price validation
	if (!is_numeric($price)) {
		$err['price'] = true;
	} elseif (intval($price) < PRICE_MIN) {
		$err['price'] = true;
	} elseif (intval($price) >= PRICE_MAX) {
		$err['priceHigh'] = true;
	}

	// condition validation
	switch ($condition) {
		case 'n':
			break;
		case 'ln':
			break;
		case 'vg':
			break;
		case 'g':
			break;
		case 'b':
			break;
		default: $err['condition'] = true;
		break;
	}

	// adding book to database
	if ($err==null) {
		$ubPin = generatePin();
		$ubid = db::addSellingBook($isbn10, $email, $zipcode, $price, $condition, $ubPin);
		if ($ubid) {
			$ub = db::getUBdoubleCheck($ubid, $ubPin);
			$sBookSaved = true;
			$subject = $ub["title"]." (".$ub["condition"]."/$".$ub["price"].")";
			$manageLink = DOMAIN."manage.php?ubid=".$ubid."&pin=".$ubPin;
			$emailArray = composeEmail($subject,'','submit',$replyLink, $manageLink);
			$success = mail($ub["email"], $subject, $emailArray["body"], $emailArray["headers"]);
		}
	} else {
		$book = db::searchByISBN($isbn10);
	}
}
// END seller form submitted

// buyer form submitted
if (isset($_GET['bSearchFind']) || isset($_GET['bSearchShowAll'])) {

	$bSearch = $_GET['bSearch'];
	if (isset($_GET['bZipcode'])) {
		$bZipcode = $_GET['bZipcode'];
	}

	// zipcode validation
	if (strlen($bZipcode) <= 0 ||  strlen($bZipcode) > 6) {
		$err['bZipcode'] = true;
	} else if (!db::validZipcode($bZipcode)) {
		$err['bZipcode'] = true;
	}

	// searched title validation
	if (isset($_GET['bSearchFind'])) {
		if (strlen($bSearch) <= 0 || strlen($bSearch) > BSEARCH_MAX_LEN) {
			$err['bSearch'] = true;
		}
	} else {
		$bSearch = '';
	}
}
// END buyer form submitted

?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="description" content="Buy and sell books to people living around you." /> <meta name="keywords" content="sell books, buy books, trade books, exchange books, meyoubook" />
<link rel="stylesheet" type="text/css" href="css/index.css">
<link rel="icon" href="icon/favicon.ico" type="image/x-icon">
<title>MeYouBook</title>

<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<script type="text/javascript" src="javascripts/index.js"></script>
<script type="text/javascript"
	src="http://www.google.com/recaptcha/api/js/recaptcha_ajax.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-28582022-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<!--<script type="text/javascript">-->
<!-- var RecaptchaOptions = {-->
<!--    theme : 'custom',-->
<!--    custom_theme_widget: 'recaptcha_widget'-->
<!-- };-->
<!-- </script>-->
<body id="main">

<div id="screen2">
<div id="screen2l2">
<div id="screen2l3">
<div id="sendEmailForm">
<div id="bSubjectContainer" class="formField">
<div id="bEmailIntro">Sending message to the book owner:</div>
<label class="formLabel">Subject</label><span id="subject"></span></div>
<div id="messageContainer" class="formField">
<div><label id="messageLabel" class="formLabel">Message</label><textarea
	id="message" onkeypress="return imposeMaxLength(this, <?php echo EMAILBODY_MAX_LEN; ?>);"
	class="inputText" name="message"></textarea></div>
<div id="bMessageNote" class="note">Please type your message to the
book owner. (max length: <?php echo EMAILBODY_MAX_LEN;?> characters)</div>
</div>
<div id="bEmailContainer" class="formField">
<div><label class="formLabel">Your email</label><input id="bEmail"
	class="inputText" name="bEmail" type="text"
	maxlength="<?php echo EMAIL_MAX_LEN;?>"></div>
<div id="bEmailNote" class="note">Please type your valid email.</div>
</div>
<div class="formField captchaContainer">
<div style="color: #47695B; font-size: 18px; margin-top: 20px;">Please
type the two words in the box with a space in between. (non-case
sensitive)</div>
<div id="bCaptcha" class="captcha"></div>
<div id="bCaptchaNote" class="note">Please type the two words you see
above.</div>

</div>
<div style="margin-top: 15px"><input id="bSubmit" class="button"
	name="bSubmit" type="button" value="Send"><input id="bCancel"
	class="button" name="bCancel" type="button" value="Cancel"><input
	id="ubid" name="ubid" value="" type="hidden"></div>
<div id="messageStatus">Sending...</div>
</div>
</div>
</div>
</div>
<div id="content"><?php echo file_get_contents('header.html'); ?>
<p id="intro">Buy and sell books to people living around you.</p>
<div id="maintabs">
<ul>
	<li>
	<div id="stabtop"
		class="tabtop <?php if (!isset($_GET['bSearchFind']) &&  !isset($_GET['bSearchShowAll'])) {echo 'hiback';}?>"></div>
	<div id="sButton"
		class="maintab <?php if (!isset($_GET['bSearchFind']) &&  !isset($_GET['bSearchShowAll'])) {echo 'hifont';}?>">Sell</div>
	</li>
	<li>
	<div id="btabtop"
		class="tabtop <?php if (isset($_GET['bSearchFind']) ||  isset($_GET['bSearchShowAll'])) {echo 'hiback';}?>"></div>
	<div id="bButton"
		class="maintab <?php if (isset($_GET['bSearchFind']) ||  isset($_GET['bSearchShowAll'])) {echo 'hifont';}?>bsearch
		">Buy</div>
	</li>
</ul>
</div>
<div id="formsContainer">
<div>
<div id="sFormContainer"
<?php if (isset($_GET['bSearchFind']) || isset($_GET['bSearchShowAll'])) {echo 'style="display: none"';}?>>
<form id="sForm" action="index.php" method="post">
<div class="formField">
<div id="sSearchContainer">
<div><label class="formLabel">ISBN</label><input id="sSearch" maxlength="20"
	autofocus="autofocus" class="inputText" name="sSearch" type="text"
	value="<?php echo ((isset($_POST['sSubmit']) && !isset($sBookSaved)) ? $_POST['sSearch'] : ''); ?>"><img
	id="sLoading" class="loading" alt="" src="icon/loading.gif"></div>
<div id="sISBNNote" class="note"
<?php echo (isset($err['isbn']) ? ' style="display:block"' : 'style="display:none"'); ?>>Please
use a valid ISBN.</div>
<div style="margin: 10px 0 0 100px; color:gray; font-size: 10px;">Insert the ISBN of the book you are selling. (test ISBN: 1118441540)</div>
</div>
</div>


<div id="sResults"
<?php echo ((isset($_POST['sSubmit']) && !isset($sBookSaved)) ? ' style="display:block"' : 'style="display:none"'); ?>>
<div id="sBookInfoContainer"><img id="cover" class="cover" alt=""
	src="<?php echo "covers/".$book["frontCover"]; ?>">
<table class="bookInfo">
	<tr>
		<td class="l1">Title:</td>
		<td id="title" class="bookInfoItem"><?php echo ((isset($_POST['sSubmit']) && !isset($sBookSaved)) ? $book["title"] : ''); ?></td>
	</tr>
	<tr>
		<td class="l1">Author(s):</td>
		<td id="authors" class="bookInfoItem""><?php echo ((isset($_POST['sSubmit']) && !isset($sBookSaved)) ? $book["authors"] : ''); ?></td>
	</tr>
	<tr>
		<td class="l1">Publisher:</td>
		<td id="publisher" class="bookInfoItem""><?php echo ((isset($_POST['sSubmit']) && !isset($sBookSaved)) ? $book["publisher"] : ''); ?></td>
	</tr>
	<tr>
		<td class="l1">Length:</td>
		<td id="length" class="bookInfoItem"><?php echo ((isset($_POST['sSubmit']) && !isset($sBookSaved)) ? $book["length"] : ''); ?></td>

	</tr>
	<tr>
		<td class="l1">ISBN:</td>
		<td id="isbn" class="bookInfoItem""><?php echo ((isset($_POST['sSubmit']) && !isset($sBookSaved)) ? $book["isbn"] : ''); ?></td>
	</tr>
</table>

</div>
</div>
<?php if (isset($sBookSaved)) {echo '<p id="sBookSaved" class="note2">Your book was successfully saved. You will receive messages from people interested in buying it at your email.</p>';} ?>
<div id="sFormContinue"
<?php echo (isset($_POST['sSubmit']) && !isset($sBookSaved) ? 'style="display:block"' : 'style="display:none"'); ?>>

<div id="sEmailContainer" class="formField">
<div><label class="formLabel">Email</label><input id="sEmail"
	maxlength="<?php echo EMAIL_MAX_LEN;?>" class="inputText" name="sEmail"
	type="text"
	value="<?php echo (isset($_POST['sSubmit']) ? $email : ''); ?>"></div>
<div id="sEmailNote" class="note"
	<?php echo (isset($err['email']) || isset($err['emailLong']) ? ' style="display:block"' : 'style="display:none"'); ?>><?php echo (isset($err['email'])?'Please type your valid email.' : ''); echo (isset($err['emailLong'])?'Sorry, email address must be less than 100 character long.' : ''); ?></div>
</div>

<div id="sConditionContainer" class="formField">
<div><label class="formLabel">Condition</label> <select id="sCondition"
	class="inputText" name="sCondition">
	<option value="-"
	<?php echo (((isset($_POST['sSubmit']) && !isset($sBookSaved)) && $condition=="-") ? 'selected' : ''); ?>>-</option>
	<option value="n"
	<?php echo (((isset($_POST['sSubmit']) && !isset($sBookSaved)) && $condition=="n") ? 'selected' : ''); ?>>New</option>
	<option value="ln"
	<?php echo (((isset($_POST['sSubmit']) && !isset($sBookSaved)) && $condition=="ln") ? 'selected' : ''); ?>>Like
	new</option>
	<option value="vg"
	<?php echo (((isset($_POST['sSubmit']) && !isset($sBookSaved)) && $condition=="vg") ? 'selected' : ''); ?>>Very
	good</option>
	<option value="g"
	<?php echo (((isset($_POST['sSubmit']) && !isset($sBookSaved)) && $condition=="g") ? 'selected' : ''); ?>>Good</option>
	<option value="b"
	<?php echo (((isset($_POST['sSubmit']) && !isset($sBookSaved)) && $condition=="b") ? 'selected' : ''); ?>>Bad</option>
</select></div>
<div id="sConditionNote" class="note"
<?php echo (isset($err['condition']) ? ' style="display:block"' : 'style="display:none"'); ?>>Please
choose your book condition.</div>
</div>

<div id="sPriceContainer" class="formField">
<div><label class="formLabel" style="width: 79px !important;">Price</label><label
	id="dollorSign">$</label><input id="sPrice" maxlength="6"
	class="inputText" name="sPrice" type="text"
	value="<?php echo ((isset($_POST['sSubmit']) && !isset($sBookSaved)) ? $price : ''); ?>"></div>
<div id="sPriceNote" class="note"
<?php echo (isset($err['price']) || isset($err['priceHigh']) ? ' style="display:block"' : 'style="display:none"'); ?>><?php if($err['price']) echo 'Please
type a valid price.'; elseif($err['priceHigh']) echo 'Sorry, you can\'t sell your car here. Please choose a price less than $'.PRICE_MAX.'.';?></div>
</div>

<div id="sZipCodeContainer" class="formField">
<div><label class="formLabel">Zip code</label><input id="sZipCode"
	class="inputText" name="sZipcode" type="text" maxlength="5"
	value="<?php echo (isset($_POST['sSubmit']) ? $zipcode : ''); ?>"><input
	id="sISBN" type="hidden" name="sISBN"
	value="<?php echo isset($_POST['sSubmit'])?$isbns:''; ?>"></div>
<div id="sZipNote" class="note"
<?php echo (isset($err['sZipcode']) ? ' style="display:block"' : 'style="display:none"'); ?>>Please
type your valid zip code. <a href="<?php echo "faq.php?q=9#9" ?>">Having problems?</a></div>
</div>

<div class="formField captchaContainer">
<div style="color: #47695B; font-size: 18px; margin-top: 20px;">Please
type the two words in the box with a space in between. (non-case
sensitive)</div>
<div id="sCaptcha" class="captcha">
<table style="padding: 10px 0; margin-left: -2px;">
	<tr>
		<td>
		<div id="recaptcha_image"></div>
		</td>
		<td style="padding: 10px;">

		<div class="captchaOpt"><a tabindex="-1" id="recaptcha_reload_btn"
			title="Get a new challenge" href="javascript:Recaptcha.reload();"><img
			id="recaptcha_reload" width="25" height="17"
			src="http://www.google.com/recaptcha/api/img/white/refresh.gif"
			alt="Get a new challenge"></a></div>
		<div class="captchaOpt"><a tabindex="-1"
			id="recaptcha_switch_audio_btn" class="recaptcha_only_if_image"
			title="Get an audio challenge"
			href="javascript:Recaptcha.switch_type('audio');"><img
			id="recaptcha_switch_audio" width="25" height="16"
			alt="Get an audio challenge"
			src="http://www.google.com/recaptcha/api/img/white/audio.gif"></a><a
			tabindex="-1" id="recaptcha_switch_img_btn"
			class="recaptcha_only_if_audio" title="Get a visual challenge"
			href="javascript:Recaptcha.switch_type('image');"><img
			id="recaptcha_switch_img" width="25" height="16"
			alt="Get a visual challenge"
			src="http://www.google.com/recaptcha/api/img/white/text.gif"></a></div>
		<div class="captchaOpt"><a tabindex="-1" id="recaptcha_whatsthis_btn"
			title="Help"
			href="http://www.google.com/recaptcha/help?c=03AHJ_VuvpzrX5I16UtxMWRpgKZ-r8x8DCl6Xvk5wKRIs1VpMJJQ3Y3sdJr5xLMEmqbcZY_KDnXvGeqgwKZhPqj_lo81nNiPYJxxjdvB542vELUTTeqKKR0WjvRWVb6BEU_qbV6npr-LvlD6XgjgoMZuibyO_O8WL8_A"
			target="_blank"><img id="recaptcha_whatsthis" width="25" height="16"
			src="http://www.google.com/recaptcha/api/img/white/help.gif"
			alt="Help"></a></div>

		</td>
		<td><img src="http://www.google.com/recaptcha/api/img/clean/logo.png"
			width="71px" height="36px" alt="Powered by reCAPTCHA"
			title="Powered by reCAPTCHA"></td>
	</tr>
</table>


<input type="text" name="recaptcha_response_field" maxlength="50"
	id="recaptcha_response_field" class="inputText" size="30" /></div>
<div id="sCaptchaNote" class="note"
<?php echo (isset($_POST['sSubmit']) && !isset($sBookSaved) ? 'style="display:block"' : 'style="display:none"'); ?>>Please
type the two words you see in the above box.</div>
</div>

<div id="sButtonContainer" class="formField"><input id="sSubmit"
	class="button" name="sSubmit" type="submit" value="Submit"><span
	id="submittingStatus">Submitting...</span></div>
</div>
</form>

</div>
<div id="bFormContainer"
<?php if (!isset($_GET['bSearchFind']) && !isset($_GET['bSearchShowAll'])) {echo ' style="display: none"';}?>>
<form id="bForm" action="index.php" method="get">
<div id="bSearchContainer">
<div class="formField">
<div><label class="formLabel">Book Title</label><input id="bSearch"
	class="inputText" name="bSearch" autofocus="autofocus"
	maxlength="<?php echo BSEARCH_MAX_LEN; ?>" type="text"
	value="<?php echo (isset($bSearch)) ? $bSearch : '';?>"><img
	id="bloading" class="loading" alt="" src="icon/loading.gif"></div>
<div id="bSearch" class="note"
<?php echo (isset($err['bSearch']) ? ' style="display:block"' : 'style="display:none"'); ?>>Please
type the title of the book you are looking for.</div>
</div>

<div class="formField">
<div><label class="formLabel">Zip code</label><input id="bZipcode"
<?php if (isset($_GET['bSearchShowAll']) && $err['bZipcode']) echo 'autofocus="autofocus"'?>
	class="inputText" name="bZipcode" type="text" maxlength="5"
	value="<?php  echo (isset($bSearch)) ? $bZipcode : '';?>"><input
	id="bSearchFind" class="button" name="bSearchFind" type="submit"
	value="Find"><input id="bSearchShowAll" class="button"
	name="bSearchShowAll" type="submit" value="All books around me!"></div>
<div style="margin: 10px 0 0 100px; color:gray; font-size: 10px;">Insert a zip code around your place. (test zip code: 80303)</div>
<div id="bZipNote" class="note"
<?php echo (isset($err['bZipcode']) ? ' style="display:block"' : 'style="display:none"'); ?>>Please
type your valid zip code to find available books around you. <a href="<?php echo "faq.php?q=9#9" ?>">Having problems?</a></div>
</div>
</div>
</form>
</div>

<?php

if ((isset($_GET['bSearchFind']) || isset($_GET['bSearchShowAll'])) && (!isset($err['bZipcode']) && !$err['bSearch'])) {
	$result = db::searchByTitle($bSearch, $bZipcode);
	echo '<div id="bResults">';
	if (mysql_num_rows($result)>0) {

		$book=mysql_fetch_array($result);

		echo '<ul>';
		while($book) {
			$length = "n/a";
			if ($book["length"]>0) {
				$length = $book["length"]." pages";
			}

			echo '<li><div class="bBookInfoContainer">';
			echo '<div><img class="cover" alt="" src="covers/'.$book['frontcover'].'">';
			echo '<table class="bookInfo">';
			echo '<tr><td class="l1">Title:</td>';
			echo '<td class="bookInfoItem">'.$book['title'].'</td></tr>';
			echo '<tr><td class="l1">Author(s):</td>';
			echo '<td class="bookInfoItem">'.$book['authors'].'</td></tr>';
			echo '<tr><td class="l1">Publisher:</td>';
			echo '<td id="title" class="bookInfoItem">'.$book['publisher'].'</td></tr>';
			echo '<tr><td class="l1">Length:</td>';
			echo '<td class="bookInfoItem">'.$length.'</td></tr>';
			echo '<tr><td class="l1">ISBN:</td>';
			echo '<td class="bookInfoItem">'.$book['isbn10'].', '.$book['isbn13'].'</td></tr>';
			echo '<td class="bookInfoItem"><a class="moreInfo" href="http://books.google.com/books?id='.$book['bookgoogleid'].'&printsec=frontcover">More info</a></td></tr>';
			echo '</table></div>';

			echo '<div class="bBookPriceContainer"><div class="bBookPriceHeader"><span>Price</span><span>Zip Code</span><span>Condition</span></div>';
			$isbn10 = $book['isbn10'];
			while ($isbn10 == $book['isbn10']) {
				switch ($book['condition']) {
					case 'n' :
						$condition = 'New';
						break;
					case 'ln' :
						$condition = 'Like new';
						break;
					case 'vg' :
						$condition = 'Very good';
						break;
					case 'g' :
						$condition = 'Good';
						break;
					case 'b' :
						$condition = 'Bad';
						break;
				}
				echo '	<div class="bBookPriceRow"><span>$'.$book['price'].'</span><span>'.$book['zipcode'].'</span><span>'.$condition.'</span><input name="ubid" type="hidden" value="'.$book['id'].'"><span class="emailSeller">Contact owner</span></div>';
				$book = mysql_fetch_array($result);
			}
			echo '</div></div></li>';
		}
		echo '</ul>';
	} else if (!$err['bSearch'] && !$err['bZipcode']) {
		echo '<p class="note2">Sorry, no results were found.</p>';
	}
	echo '</div>';
}

?></div>
</div>
</div>

<?php echo file_get_contents('footer.html'); ?>
</body>
</html>
