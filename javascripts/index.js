$(document).ready(function() {

	Recaptcha.create("6LdJtswSAAAAAApS_lMpzwd93QSxwWbY2NtC5TP0","sCaptcha",
		{
			theme: "custom",
			callback: Recaptcha.focus_response_field
		}
	);	
	
	$("#sSearch").bind('input', function() {
		$("#sBookSaved").hide();
		
		var isbn = $(this).val().replace(/-/g, '').replace(/ /g, '');
		
		if (isbn.length == 10 || isbn.length == 13) {
						
			$("#sLoading").css("visibility", "visible");
			$("#sNote").hide();
			$("#sResults").hide();
			$("#sFormContinue").hide();
			$(".note").hide();

			$.getJSON("ajax/searchBook.php", {
				isbn : isbn
			}, function(book) {
				if (book.toString() != "false") { // book has found

					if (book.frontCover != "") {
						$("#cover").attr("src", book.frontCover);
					}
					if (book.title != "") {
						$("#title").html(book.title);
					} else {
						$("#title").html("n/a");
					}
					if (book.authors != "") {
						$("#authors").html(book.authors);
					} else {
						$("#authors").html("n/a");
					}
					if (book.publisher != "") {
						$("#publisher").html(book.publisher);
					} else {
						$("#publisher").html("");
					}
					if (book.length != "") {
						$("#length").html(book.length);
					}
					if (book.isbn != "") {
						$("#isbn").html(book.isbn);
						$("#sISBN").attr("value", book.isbn);
					}
					$("#sResults").show("fast");
					$("#sFormContinue").show("fast");
					$("#sEmail").focus();
				} else {
					$("#sISBNNote").html("Sorry, no book matched the ISBN number you entered. Please try again or <a href=\"faq.php?q=2#2\">learn more!</a>");
					$("#sISBNNote").show("fast");
				}
				$("#sLoading").css("visibility", "hidden");
			
			});
		}
	});
	
	$(".emailSeller").click(function() {
		
		var sCaptchaHTML = $("#sCaptcha").html();
		$("#sCaptcha").html('');
		$("#bCaptcha").html(sCaptchaHTML);		
		
		$('#message').val('');
		$('#bEmail').val('');
		$('#messageStatus').hide();
		$('#screen2').show();
		$('#message').focus();
		$('#subject').html($(this).parent().parent().parent().children()[0].children[1].children[0].children[0].children[1].innerHTML+' ('+$(this).parent().children()[2].innerHTML+'/'+$(this).parent().children()[0].innerHTML+')');
		$('#ubid').val($(this).prev().val());
	});
	
	// Return sending email and close the window
	$("#bCancel").click(function() {
		$('#screen2').hide();
		$('#subject').html('');
		$('#bCaptchaNote').hide();
		$('#bEmailNote').hide();
		
		$('#bSubmit').removeAttr('disabled');		
		
		var bCaptchaHTML = $("#bCaptcha").html();
		$("#bCaptcha").html('');
		$("#sCaptcha").html(bCaptchaHTML);
		Recaptcha.create("6LdJtswSAAAAAApS_lMpzwd93QSxwWbY2NtC5TP0","sCaptcha",
			{
				theme: "red",
				callback: Recaptcha.focus_response_field
			}
		);
	});
	
	$("#bSubmit").click(function() {
		
		$('#bCaptchaNote').hide();
		$('#bMessageNote').hide();
		$('#bEmailNote').hide();
		
		var ubid = $('#ubid').val();
		var message = $('#message').val();
		var bEmail = $('#bEmail').val();
		var recaptcha_challenge_field = $('#recaptcha_challenge_field').val();
		var recaptcha_response_field = $('#recaptcha_response_field').val();		
		
		$('#messageStatus').html('Sending...');
		$('#messageStatus').show();
		$('#bSubmit').attr('disabled','disabled');
		$.post("ajax/email.php", {
			ubid : ubid, message : message, bEmail : bEmail, recaptcha_challenge_field : recaptcha_challenge_field, recaptcha_response_field: recaptcha_response_field, bSubmit : true
		}, function(response) {
			$('#messageStatus').hide();
			if (response == true) {
				$('#messageStatus').html('Your message has been sent to the book owner.');
				$('#messageStatus').fadeIn();
				window.setTimeout("$('#bCancel').click();", 4000);
			} else {
				Recaptcha.reload("t");
				$('#bSubmit').removeAttr('disabled');			
				if (response.captcha) { 
					$('#bCaptchaNote').show('fast');					
				}
				if (response.insert) {
					$('#messageStatus').html('You already sent a message to the owner of this book. Responds from the owner will be forwarded to your email address.');
					$('#messageStatus').fadeIn();
					window.setTimeout("$('#bCancel').click();", 12000);
				}
				if (response.server) {
					$('#messageStatus').html('Sorry, we are having difficulty sending your message. Please try again later.');
					$('#messageStatus').fadeIn();
					window.setTimeout("$('#bCancel').click();", 12000);
				}
				if (response.message) {
					$('#bMessageNote').show('fast');
				}
				if (response.email) {
					$('#bEmailNote').show('fast');
				}				
			}
		}, "json");
	});

	// toggle main tabs
	$("#bButton").click(function() {
		$("#sBookSaved").hide();
		
		// interface toggle
		$(this).addClass("hifont");
		$("#btabtop").addClass("hiback");
		$("#sButton").removeClass("hifont");
		$("#stabtop").removeClass("hiback");

		$("#sResults").hide();
		$("#bResults").show();

		// form toggle
		$('#sFormContainer').hide();
		$('#bFormContainer').show();
		
		$('#bSearch').focus();
	});

	// toggle main tabs
	$("#sButton").click(function() {
		$("#sBookSaved").hide();
		
		// interface toggle
		$(this).addClass("hifont");
		$("#stabtop").addClass("hiback");
		$("#bButton").removeClass("hifont");
		$("#btabtop").removeClass("hiback");

		$("#bResults").hide();
		if ($("#sFormContinue").css("display") != "none") {
			$("#sResults").show();
		}

		// form toggle
		$('#bFormContainer').hide();
		$('#sFormContainer').show();
		
		$('#sSearch').focus();
	});
	
	$("#bSearchFind").click(function() {
		var title = $("#bSearch").val();
		if (title.len == 0) {
			
			$("#bSearchNote").html("Please type the title of the book you are looking for.");
		}
	});
	
	$("#sSubmit").click(function() {
		$(this).hide();
		$("#submittingStatus").fadeIn('fast');
	});
});

function imposeMaxLength(Object, MaxLen)
{
  return (Object.value.length <= MaxLen);
}
