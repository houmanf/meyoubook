$(document).ready(function() {
	
	$("#send").click(function() {
		
		$('#messageNote').hide();
		$('#emailNote').hide();
		
		var message = $('#message').val();
		
		$('#messageStatus').html('Sending...');
		$('#messageStatus').show();
		$('#send').attr('disabled','disabled');
		$.post("ajax/email.php", {
			message : message, send : true
		}, function(response) {
			$('#messageStatus').hide();
			if (response == true) {
				$('#messageStatus').html('Your message has been sent. You may now close this window.');
				$('#messageStatus').fadeIn();
			} else {			
				$('#send').removeAttr('disabled');
				if (response.server) {
					$('#messageStatus').html('Sorry, we are having difficulty sending your message. Please try again later.');
					$('#messageStatus').fadeIn();
				}
				if (response.message) {
					$('#messageNote').show('fast');
				}
				if (response.email) {
					$('#emailNote').show('fast');
				}				
			}
		}, "json");
	});	
});